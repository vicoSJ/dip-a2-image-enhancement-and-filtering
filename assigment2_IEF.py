## =======================================================================================
# Name:        Victor Hugo Sillerico Justo
# No. USP:     11904461
# Course Code: SCC5830 (Image Processing)
# Year:        2020
# Semester:    I
# Assigment 2: Image Enhancement and Filtering
# Git repository URL:    https://gitlab.com/vicoSJ/dip-a2-image-enhancement-and-filtering
## =======================================================================================


import numpy as np
import imageio
import math

# INPUT data
filename = str(input()).rstrip()
input_img = imageio.imread(filename)
method = int(input())
save = int(input())

# Convert input data (image) to the appropriate format
input_img = input_img.astype(np.float)

# ********** AUXILIAR FUNCTIONS TO ADD & REMOVE PADDING **********
# Function to Add Padding to the INPUT image
def addPadding(input_img,N,M,n):
    indI =  int((n-1)/2) # auxiliar variable to get the extra rows & columns to add
    imgp = np.zeros((N+(2*indI),M+(2*indI))) 
    for i in range(indI,N+indI):
        for j in range(indI,M+indI):
            imgp[i,j] = input_img[i-indI,j-indI] # copy the original image inside the expanded image
    return imgp

# Function to Remove Padding from the OUTPUT image
def removePadding(input_img,N,M,n):
    indI =  int((n-1)/2) # auxiliar variable to get the extra rows & columns to remove
    imgwp = np.zeros((N,M))
    for i in range(indI,N+indI):
        for j in range(indI,M+indI):
            imgwp[i-indI,j-indI] = input_img[i,j] # recover the original image from the expanded image
    return imgwp

# ********** FUNCTIONS TO APPLY BILATERAL FILTERING **********
# auxiliar function to calulate the Euclidean distance between the position of the intensity
# and the center (0,0)
def Edist(n):
    aux = int((n-1)/2) # auxiliar variable to center the filter at the origin
    E = np.zeros((n,n))
    for i in range(0, n):
        for j in range(0, n):    
            x = i-aux
            y = j-aux
            E[i,j] = math.sqrt((x**2)+(y**2))
    return E

# auxiliar function to calculate differences on intensities for each neighborhood
def difIntensity(I,n):
    difI = np.zeros((n,n))
    c = int((n-1)/2)  
    Ixy = I[c,c]  # pixel in the center
    Ixy = np.full((n,n), I[c,c])
    difI = np.subtract(I,Ixy) # to get:  "I - I(x,y)"
    return difI 

# auxiliar function to calculate the NEW value of intensity for a pixel
def newIntensity_i(I,n,sig_r,gs):
    Xgri = difIntensity(I,n)
    gr = (1/(2*np.pi*sig_r**2))*np.exp(-(Xgri**2)/(2*sig_r**2)) # get the RANGE GAUSSIAN component
    w = np.multiply(gr,gs) # get the total value of the filter
    Wp = np.sum(w) # get the normalization factor Wp
    Ifi = np.multiply(w,I)
    If = np.sum(Ifi) # total value of the NEW intensity 
    Ifp = If/Wp # apply the normalization
    return Ifp

# MAIN function to calculate the modified the image using the BILATERAL FILTER
def BilateralFilter(img, n, sig_r, gs):
    # add padding to apply the filter to ALL pixels
    Nprev,Mprev = img.shape
    input_img = addPadding(img,Nprev,Mprev,n)
    # iniciate the NEW filtered image
    imgFiltered = np.zeros(input_img.shape)
    N,M = input_img.shape
    a = int((n-1)/2) # auxiliar variable to center the filter at the origin
    for x in range(a, N-a):
        for y in range(a, M-a):
            region_img = input_img[ x-a : x+(a+1), y-a : y+(a+1)] # get the region in which the filtered will be applied
            imgFiltered[x,y] = newIntensity_i(region_img,n,sig_r,gs) # store the NEW intensity value
    imgFinal = removePadding(imgFiltered,Nprev,Mprev,n) # remove the padding from the filtered image      
    return imgFinal

# ********** FUNCTION TO APPLY UNSHARP MARK USING LAPLACIAN FILTER **********
# Function to apply Unsharp Mask using the Laplacian Filter
def unsharpMaskLF(img,c,k):
    Nprev,Mprev = img.shape
    input_img = addPadding(img,Nprev,Mprev,3) # add padding to apply the filter to ALL pixels 
    imgMod = np.zeros(input_img.shape)
    N,M = input_img.shape
    # select the kind of KERNEL
    if(k == 1):
        kernel = np.matrix([[0, -1, 0], [-1, 4, -1], [0, -1, 0]])
    if(k == 2):
        kernel = np.matrix([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]])
		# apply the filter for each pixel using its corresponding neighborhood
    for x in range(1, N-1):
        for y in range(1, M-1):
            region_img = input_img[ x-1 : x+(1+1), y-1 : y+(1+1)] 
            newRegVal = np.multiply(region_img,kernel) # apply the kernel
            newIntensityVal = np.sum(newRegVal)
            imgMod[x,y] = newIntensityVal # new filtered image
    
    imgFiltered = removePadding(imgMod,N-2,M-2,3) # recover the original image
    # scaling the filtered image
    minImg = np.min(imgFiltered)
    maxImg = np.max(imgFiltered)
    imgScaled = (imgFiltered-minImg)*255/(maxImg-minImg)
    # adding the filtered-scaled image to the original image
    imgFinal = (c*imgScaled)+img
    # scaling the final image
    minImgFinal = np.min(imgFinal)
    maxImgFinal = np.max(imgFinal)
    imgFinalScaled = (imgFinal-minImgFinal)*255/(maxImgFinal-minImgFinal)
    return imgFinalScaled


# ********** FUNCTION TO APPLY VIGNETTE FILTER **********
# Function to apply Vignette Filter
def vignetteFilter(img, sig_row, sig_col):
    R,C = img.shape
		# define the auxiliar matrix to get the 1D Gaussian Kernel
    Wrow = np.zeros((1,R))
    Wcol = np.zeros((1,C))
    # auxiliar variable to center the kernel at the origin    
    nR = int((R-1)/2)
    nC = int((C-1)/2)
    # get the 1D Gaussian Kernel associated to the ROWs
    for i in range(0,R):
        x = i - nR
        # calculate the 1D Gaussian
        Wrow[0,i] = (1/(2*np.pi*sig_row**2))*np.exp(-(x**2)/(2*sig_row**2))
		# get the 1D Gaussian Kernel associated to the COLUMNs
    for i in range(0,C):
        x = i - nC
        # calculate the 1D Gaussian
        Wcol[0,i] = (1/(2*np.pi*sig_col**2))*np.exp(-(x**2)/(2*sig_col**2))
    # get the total KERNEL
    W = np.multiply(Wrow.transpose(),Wcol)
    # apply the filter
    imgFiltered = np.multiply(img,W)
    # Scaling the filtered image
    minImg = np.min(imgFiltered)
    maxImg = np.max(imgFiltered)
    imgScaled = (imgFiltered-minImg)*255/(maxImg-minImg)
    return imgScaled

# Function to calculate the Root Square Error (RSE)   
def RSE(r_img,m_img):
    s=0
    r,c = r_img.shape
    # Comparing each pixel to get the error
    for i in np.arange(r):
        for j in np.arange(c):
            s = s+(m_img[i,j]-r_img[i,j])**2
    return np.sqrt(s)
		
# ***************====== SELECTION OF IMAGE PROCESSING ======***************
if method == 1:
    #Getting the parameters to make the transformation
		n = int(input())
		sig_s = float(input())
		sig_r = float(input())
		# calculate the SPATIAL GAUSSIAN COMPONENT
		X = Edist(n)
		gs = (1/(2*np.pi*sig_s**2))*np.exp(-(X**2)/(2*sig_s**2))	
		output_img = BilateralFilter(input_img, n, sig_r, gs) 

if method == 2:
    # Getting the parameters to make the transformation
		c = float(input())
		k = int(input())
		output_img = unsharpMaskLF(input_img, c, k)

if method == 3:
    # Getting the parameters to make the transformation
		sig_row = float(input())
		sig_col = float(input())
		output_img = vignetteFilter(input_img, sig_row, sig_col)

# Option to SAVE the modified image    
if save == 1:
    save_img = output_img.astype(np.uint8)
    imageio.imwrite("output_img.png",save_img)

# Getting the Root Square Error (RSE) with 4 decimals
val_rse = RSE(input_img,output_img)
val_rse = np.around(val_rse,decimals=4)
print(val_rse)






